import type { Config } from 'tailwindcss'
import { fontFamily } from 'tailwindcss/defaultTheme'

const config = {
    darkMode: ['class'],

    content: [
        './pages/**/*.{ts,tsx}',
        './components/**/*.{ts,tsx}',
        './app/**/*.{ts,tsx}',
        './src/**/*.{ts,tsx}',
    ],

    prefix: '',

    theme: {
        container: {
            center: true,
            padding: '2rem',

            screens: {
                '2xl': '1400px',
            },
        },

        extend: {
            colors: {
                border: 'hsl(var(--border))',
                input: 'hsl(var(--input))',
                ring: 'hsl(var(--ring))',

                background: 'hsl(var(--background))',
                foreground: 'hsl(var(--foreground))',

                primary: {
                    DEFAULT: 'hsl(var(--primary))',
                    foreground: 'hsl(var(--primary-foreground))',
                },

                secondary: {
                    DEFAULT: 'hsl(var(--secondary))',
                    foreground: 'hsl(var(--secondary-foreground))',
                },

                destructive: {
                    DEFAULT: 'hsl(var(--destructive))',
                    foreground: 'hsl(var(--destructive-foreground))',
                    text: 'hsl(var(--destructive-text))',
                },

                muted: {
                    DEFAULT: 'hsl(var(--muted))',
                    foreground: 'hsl(var(--muted-foreground))',
                },

                accent: {
                    DEFAULT: 'hsl(var(--accent))',
                    foreground: 'hsl(var(--accent-foreground))',
                },

                popover: {
                    DEFAULT: 'hsl(var(--popover))',
                    foreground: 'hsl(var(--popover-foreground))',
                },

                card: {
                    DEFAULT: 'hsl(var(--card))',
                    foreground: 'hsl(var(--card-foreground))',
                },
            },

            gridTemplateColumns: {
                dashboard: 'repeat(auto-fit, minmax(min(100%, 400px), 1fr))',
            },

            height: {
                header: 'var(--header-height)',
            },

            minHeight: {
                available: 'calc(100vh - var(--header-height))',
            },

            fontFamily: {
                sans: ['var(--font-sans)', ...fontFamily.sans],
            },

            borderRadius: {
                lg: 'var(--radius)',
                md: 'calc(var(--radius) - 2px)',
                sm: 'calc(var(--radius) - 4px)',
            },

            transitionTimingFunction: {
                'elastic-out':
                    'linear(0,0.2178 2.1%,1.1144 8.49%,1.2959 10.7%,1.3463 11.81%,1.3705 12.94%,1.3726,1.3643 14.48%,1.3151 16.2%,1.0317 21.81%,0.941 24.01%,0.8912 25.91%,0.8694 27.84%,0.8698 29.21%,0.8824 30.71%,1.0122 38.33%,1.0357,1.046 42.71%,1.0416 45.7%,0.9961 53.26%,0.9839 57.54%,0.9853 60.71%,1.0012 68.14%,1.0056 72.24%,0.9981 86.66%,1)',
            },

            keyframes: {
                'spin-in': {
                    from: {
                        transform: 'rotate(0deg)',
                    },

                    to: {
                        transform: 'rotate(180deg)',
                    },
                },

                'accordion-down': {
                    from: {
                        height: '0',
                    },

                    to: {
                        height: 'var(--radix-accordion-content-height)',
                    },
                },

                'accordion-up': {
                    from: {
                        height: 'var(--radix-accordion-content-height)',
                    },

                    to: {
                        height: '0',
                    },
                },
            },

            animation: {
                'spin-in': 'spin-in 400ms linear',
                'accordion-down': 'accordion-down 200ms ease-out',
                'accordion-up': 'accordion-up 200ms ease-out',
            },
        },
    },

    plugins: [require('tailwindcss-animate')],
} satisfies Config

export default config
