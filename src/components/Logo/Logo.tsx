import Link from 'next/link'

import { PuzzlePieceIcon } from '@heroicons/react/24/outline'

import { routes } from '@/config/routes'

export default function Logo() {
    return (
        <Link className={'rounded-sm focus-ring'} href={routes.home.pathname}>
            <PuzzlePieceIcon className={'size-6'} />
            <span className={'sr-only'}>Cyber-Treasure</span>
        </Link>
    )
}
