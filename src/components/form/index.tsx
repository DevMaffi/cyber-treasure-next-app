import React from 'react'
import { useFormStatus } from 'react-dom'

import type { VariantProps } from 'class-variance-authority'
import { cva } from 'class-variance-authority'

import { Loader2 } from 'lucide-react'

import { Button } from '@/components/ui/button'

import { cn } from '@/lib/utils'

type FormItemProps = Readonly<{
    children: React.ReactNode
    className?: string
}>

export function FormItem(props: FormItemProps) {
    const { children, className } = props

    return <div className={cn('space-y-2', className)}>{children}</div>
}

const formMessageVariants = cva('select-none', {
    variants: {
        variant: {
            default: 'text-muted-foreground',
            destructive: 'text-destructive-text',
        },
    },
    defaultVariants: {
        variant: 'default',
    },
})

type FormMessageProps = Readonly<
    VariantProps<typeof formMessageVariants> & {
        className?: string
        message?: string | string[]
    }
>

export function FormMessage(props: FormMessageProps) {
    const { className, variant, message } = props

    if (!message) {
        return null
    }

    return (
        <p
            className={cn(
                formMessageVariants({
                    variant,
                    className,
                }),
            )}
        >
            {message}
        </p>
    )
}

type FormButtonGroupProps = Readonly<{
    children: React.ReactNode
}>

export function FormBtnGroup(props: FormButtonGroupProps) {
    const { children } = props

    return <div className={'flex items-center gap-2.5'}>{children}</div>
}

type SubmitButtonProps = Readonly<{
    label: string
}>

export function SubmitButton(props: SubmitButtonProps) {
    const { label } = props

    const { pending } = useFormStatus()

    return (
        <Button type={'submit'} size={'sm'} disabled={pending}>
            {pending && <Loader2 className={'size-4 mr-2 animate-spin'} />}
            <span>{label}</span>
        </Button>
    )
}
