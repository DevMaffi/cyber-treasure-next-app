'use client'

import { useRootShortcuts, useStoreShortcuts } from '@/hooks'

export function RootShortcuts() {
    useRootShortcuts()

    return null
}

export function StoreShortcuts() {
    useStoreShortcuts()

    return null
}
