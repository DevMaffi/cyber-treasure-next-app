import React from 'react'

import { cn } from '@/lib/utils'

type TypographyProps = Readonly<React.ComponentProps<'div'>>

export function Typography(props: TypographyProps) {
    const { children, className, ...restProps } = props

    return (
        <div className={cn('flow', className)} {...restProps}>
            {children}
        </div>
    )
}

type TypographyH1Props = Readonly<React.ComponentProps<'h1'>>

export function TypographyH1(props: TypographyH1Props) {
    const { children, className, ...restProps } = props

    return (
        <h1
            className={cn(
                'scroll-m-20 text-4xl font-extrabold tracking-tight lg:text-5xl',
                className,
            )}
            {...restProps}
        >
            {children}
        </h1>
    )
}

type TypographyH2Props = Readonly<React.ComponentProps<'h2'>>

export function TypographyH2(props: TypographyH2Props) {
    const { children, className, ...restProps } = props

    return (
        <h2
            className={cn(
                'scroll-m-20 border-b pb-2 text-3xl font-semibold tracking-tight first:mt-0',
                className,
            )}
            {...restProps}
        >
            {children}
        </h2>
    )
}

type TypographyH3Props = Readonly<React.ComponentProps<'h3'>>

export function TypographyH3(props: TypographyH3Props) {
    const { children, className, ...restProps } = props

    return (
        <h3
            className={cn(
                'scroll-m-20 text-2xl font-semibold tracking-tight',
                className,
            )}
            {...restProps}
        >
            {children}
        </h3>
    )
}

type TypographyH4Props = Readonly<React.ComponentProps<'h4'>>

export function TypographyH4(props: TypographyH4Props) {
    const { children, className, ...restProps } = props

    return (
        <h4
            className={cn(
                'scroll-m-20 text-xl font-semibold tracking-tight',
                className,
            )}
            {...restProps}
        >
            {children}
        </h4>
    )
}

type TypographyParagraphProps = Readonly<React.ComponentProps<'p'>>

export function TypographyP(props: TypographyParagraphProps) {
    const { children, className, ...restProps } = props

    return (
        <p className={cn('leading-7 mt-6', className)} {...restProps}>
            {children}
        </p>
    )
}

type TypographyBlockquoteProps = Readonly<React.ComponentProps<'blockquote'>>

export function TypographyBlockquote(props: TypographyBlockquoteProps) {
    const { children, className, ...restProps } = props

    return (
        <blockquote
            className={cn('mt-6 border-l-2 pl-6 italic', className)}
            {...restProps}
        >
            {children}
        </blockquote>
    )
}

type TypographyListProps = Readonly<React.ComponentProps<'ul'>>

export function TypographyList(props: TypographyListProps) {
    const { children, className, ...restProps } = props

    return (
        <ul
            className={cn('my-6 ml-6 list-disc [&>li]:mt-2', className)}
            {...restProps}
        >
            {children}
        </ul>
    )
}

type TypographyListItemProps = Readonly<React.ComponentProps<'li'>>

export function TypographyListItem(props: TypographyListItemProps) {
    const { children, ...restProps } = props

    return <li {...restProps}>{children}</li>
}

type TypographyInlineCodeProps = Readonly<React.ComponentProps<'code'>>

export function TypographyInlineCode(props: TypographyInlineCodeProps) {
    const { children, className, ...restProps } = props

    return (
        <code
            className={cn(
                'relative rounded bg-muted px-[0.3rem] py-[0.2rem] font-mono text-sm font-semibold',
                className,
            )}
            {...restProps}
        >
            {children}
        </code>
    )
}

type TypographyKbdProps = Readonly<React.ComponentProps<'kbd'>>

export function TypographyKbd(props: TypographyKbdProps) {
    const { children, className, ...restProps } = props

    return (
        <kbd
            className={cn(
                'pointer-events-none inline-flex h-5 select-none items-center gap-1 rounded border bg-muted px-1.5 font-mono text-[10px] font-medium text-muted-foreground opacity-100',
                className,
            )}
            {...restProps}
        >
            {children}
        </kbd>
    )
}

type TypographyLeadProps = Readonly<React.ComponentProps<'p'>>

export function TypographyLead(props: TypographyLeadProps) {
    const { children, className, ...restProps } = props

    return (
        <p
            className={cn('text-xl text-muted-foreground', className)}
            {...restProps}
        >
            {children}
        </p>
    )
}

type TypographyLargeProps = Readonly<React.ComponentProps<'div'>>

export function TypographyLarge(props: TypographyLargeProps) {
    const { children, className, ...restProps } = props

    return (
        <div className={cn('text-lg font-semibold', className)} {...restProps}>
            {children}
        </div>
    )
}

type TypographySmallProps = Readonly<React.ComponentProps<'small'>>

export function TypographySmall(props: TypographySmallProps) {
    const { children, className, ...restProps } = props

    return (
        <small
            className={cn('text-sm font-medium leading-none', className)}
            {...restProps}
        >
            {children}
        </small>
    )
}

type TypographyMutedProps = Readonly<React.ComponentProps<'p'>>

export function TypographyMuted(props: TypographyMutedProps) {
    const { children, className, ...restProps } = props

    return (
        <p
            className={cn('text-sm text-muted-foreground', className)}
            {...restProps}
        >
            {children}
        </p>
    )
}
