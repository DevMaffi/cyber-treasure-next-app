import React from 'react'

import { cn } from '@/lib/utils'

type TableProps = Readonly<React.ComponentProps<'table'>>

export function Table(props: TableProps) {
    const { children, className, ...restProps } = props

    return (
        <table className={cn('w-full', className)} {...restProps}>
            {children}
        </table>
    )
}

type TableHeadProps = Readonly<React.ComponentProps<'thead'>>

export function TableHead(props: TableHeadProps) {
    const { children, ...restProps } = props

    return <thead {...restProps}>{children}</thead>
}

type TableRowProps = Readonly<React.ComponentProps<'tr'>>

export function TableRow(props: TableRowProps) {
    const { children, className, ...restProps } = props

    return (
        <tr
            className={cn('m-0 border-t p-0 even:bg-muted', className)}
            {...restProps}
        >
            {children}
        </tr>
    )
}

type TableHeadingProps = Readonly<React.ComponentProps<'th'>>

export function TableHeading(props: TableHeadingProps) {
    const { children, className, ...restProps } = props

    return (
        <th
            className={cn(
                'border px-4 py-2 text-left font-bold [&[align=center]]:text-center [&[align=right]]:text-right',
                className,
            )}
            {...restProps}
        >
            {children}
        </th>
    )
}

type TableBodyProps = Readonly<React.ComponentProps<'tbody'>>

export function TableBody(props: TableBodyProps) {
    const { children, ...restProps } = props

    return <tbody {...restProps}>{children}</tbody>
}

type TableDataProps = Readonly<React.ComponentProps<'td'>>

export function TableData(props: TableDataProps) {
    const { children, className, ...restProps } = props

    return (
        <td
            className={cn(
                'border px-4 py-2 text-left [&[align=center]]:text-center [&[align=right]]:text-right',
                className,
            )}
            {...restProps}
        >
            {children}
        </td>
    )
}
