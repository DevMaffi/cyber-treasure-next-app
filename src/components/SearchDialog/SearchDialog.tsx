'use client'

import React from 'react'

import Image from 'next/image'

import { Command as CommandPrimitive } from 'cmdk'

import type { DialogProps } from '@radix-ui/react-dialog'
import * as DialogPrimitive from '@radix-ui/react-dialog'

import { MagnifyingGlassIcon } from '@heroicons/react/24/outline'
import { StarIcon, XMarkIcon } from '@heroicons/react/16/solid'

import { ShoppingBag } from 'lucide-react'

import { Dialog, DialogOverlay, DialogPortal } from '@/components/ui/dialog'
import { Command, CommandEmpty } from '@/components/ui/command'
import { Button } from '@/components/ui/button'
import { Badge } from '@/components/ui/badge'

import { TypographyKbd } from '@/components/typography'

import { cn } from '@/lib/utils'
import { formatCurrency, formatRate } from '@/lib/utils/formatters'

type SearchDialogProps = Readonly<DialogProps>

export const SearchDialog: React.FC<SearchDialogProps> = props => {
    const { children, ...restProps } = props

    return (
        <Dialog {...restProps}>
            <DialogPortal>
                <DialogOverlay />
                <DialogPrimitive.Content
                    className={
                        'fixed top-1/2 left-1/2 grid gap-4 w-full max-w-[640px] p-0 bg-background border sm:rounded-lg shadow-lg -translate-x-1/2 -translate-y-1/2 duration-200 overflow-hidden z-50 data-[state=open]:animate-in data-[state=closed]:animate-out data-[state=closed]:fade-out-0 data-[state=open]:fade-in-0 data-[state=closed]:zoom-out-95 data-[state=open]:zoom-in-95 data-[state=closed]:slide-out-to-left-1/2 data-[state=closed]:slide-out-to-top-[48%] data-[state=open]:slide-in-from-left-1/2 data-[state=open]:slide-in-from-top-[48%]'
                    }
                >
                    <Command
                        className={
                            '[&_[cmdk-group]:not([hidden])_~[cmdk-group]]:pt-0'
                        }
                    >
                        {children}
                    </Command>
                    <DialogPrimitive.Close
                        className={
                            'absolute top-4 right-4 lg:hidden rounded-sm opacity-70 transition-opacity focus-ring hover:opacity-100 disabled:pointer-events-none data-[state=open]:bg-accent data-[state=open]:text-muted-foreground'
                        }
                    >
                        <XMarkIcon className={'size-4'} />
                        <span className={'sr-only'}>Close</span>
                    </DialogPrimitive.Close>
                </DialogPrimitive.Content>
            </DialogPortal>
        </Dialog>
    )
}

type SearchInputProps = Readonly<
    React.ComponentProps<typeof CommandPrimitive.Input>
>

export const SearchInput: React.ForwardRefExoticComponent<SearchInputProps> =
    React.forwardRef((props, ref) => {
        const { className, ...restProps } = props

        return (
            <div
                className={'flex items-center gap-2 px-3 border-b'}
                cmdk-input-wrapper={''}
            >
                <MagnifyingGlassIcon className={'shrink-0 size-5 opacity-50'} />
                <CommandPrimitive.Input
                    ref={ref}
                    className={cn(
                        'flex w-full h-12 py-3 bg-transparent rounded-md outline-none text-sm placeholder:text-muted-foreground disabled:cursor-not-allowed disabled:opacity-50',
                        className,
                    )}
                    {...restProps}
                />
                <DialogPrimitive.Close
                    className={'hidden lg:inline-block'}
                    tabIndex={-1}
                >
                    <TypographyKbd className={'-translate-y-0.5'}>
                        esc
                    </TypographyKbd>
                </DialogPrimitive.Close>
            </div>
        )
    })

SearchInput.displayName = CommandPrimitive.Input.displayName

type SearchListProps = Readonly<
    React.ComponentProps<typeof CommandPrimitive.List>
>

export const SearchList: React.ForwardRefExoticComponent<SearchListProps> =
    React.forwardRef((props, ref) => {
        const { className, ...restProps } = props

        return (
            <CommandPrimitive.List
                ref={ref}
                className={cn(
                    'max-h-[500px] overflow-y-auto overflow-x-hidden',
                    className,
                )}
                data-dialog-list
                {...restProps}
            />
        )
    })

SearchList.displayName = CommandPrimitive.List.displayName

export const SearchEmpty = CommandEmpty

type SearchGroupProps = Readonly<
    React.ComponentProps<typeof CommandPrimitive.Group>
>

export const SearchGroup: React.ForwardRefExoticComponent<SearchGroupProps> =
    React.forwardRef((props, ref) => {
        const { children, className, ...restProps } = props

        return (
            <CommandPrimitive.Group
                ref={ref}
                className={cn(
                    'py-1 px-2 text-foreground overflow-hidden [&_[cmdk-group-heading]]:py-1.5 [&_[cmdk-group-heading]]:px-2 [&_[cmdk-group-heading]]:text-xs [&_[cmdk-group-heading]]:font-medium [&_[cmdk-group-heading]]:text-muted-foreground',
                    className,
                )}
                {...restProps}
            >
                <div className={'grid gap-2'}>{children}</div>
            </CommandPrimitive.Group>
        )
    })

SearchGroup.displayName = CommandPrimitive.Group.displayName

type SearchItemProps = Readonly<
    Omit<React.ComponentProps<typeof CommandPrimitive.Item>, 'children'>
> & {
    title: string
    category: string
    rate?: number
    previewUrl: string
    meta: SearchItemMetaProps
}

export const SearchItem: React.ForwardRefExoticComponent<SearchItemProps> =
    React.forwardRef((props, ref) => {
        const {
            className,
            title,
            category,
            rate,
            previewUrl,
            meta,
            ...restProps
        } = props

        const rateLabel = `${rate === undefined ? 'No reviews' : formatRate(rate)} · ${category}`

        return (
            <CommandPrimitive.Item
                ref={ref}
                className={cn(
                    'relative flex justify-between items-center gap-2.5 md:gap-5 py-1.5 px-2 rounded-sm outline-none select-none cursor-default lg:aria-selected:bg-accent lg:aria-selected:text-accent-foreground data-[disabled=true]:pointer-events-none data-[disabled=true]:opacity-50',
                    className,
                )}
                {...restProps}
            >
                <div className={'flex items-center gap-2.5'}>
                    <div
                        className={
                            'relative shrink-0 w-[102px] h-[48px] rounded-sm overflow-hidden'
                        }
                    >
                        <Image
                            className={'object-cover'}
                            src={previewUrl}
                            alt={'Game preview'}
                            fill
                        />
                    </div>
                    <div className={'grid gap-1'}>
                        <h2 className={'text-sm font-semibold truncate'}>
                            {title}
                        </h2>
                        <div
                            className={cn(
                                'flex items-center text-muted-foreground truncate',
                            )}
                        >
                            <StarIcon className={'size-3 mr-1'} />
                            <p className={'text-xs truncate'}>{rateLabel}</p>
                        </div>
                    </div>
                </div>
                <SearchItemMeta {...meta} />
            </CommandPrimitive.Item>
        )
    })

SearchItem.displayName = CommandPrimitive.Item.displayName

type SearchDialogFooterProps = Readonly<{
    children: React.ReactNode
}>

export const SearchDialogFooter = (props: SearchDialogFooterProps) => {
    const { children } = props

    return (
        <div
            className={'hidden lg:flex items-center gap-6 py-2.5 px-4 border-t'}
        >
            {children}
        </div>
    )
}

type SearchDialogCommandProps = Readonly<{
    children: React.ReactNode
    label: string
}>

export const SearchDialogCommand = (props: SearchDialogCommandProps) => {
    const { children, label } = props

    return (
        <div className={'flex items-center gap-2'}>
            <span className={'text-xs text-muted-foreground'}>{label}</span>
            <div className={'flex items-center gap-1.5'}>{children}</div>
        </div>
    )
}

const SEARCH_ITEM_META = {
    DEFAULT: 'default',
    Download: 'download',
    Installed: 'installed',
    Upcoming: 'upcoming',
} as const

type SearchItemMetaBaseProps = Readonly<{
    recent?: boolean
}>

type SearchItemMetaDefault = SearchItemMetaBaseProps &
    Readonly<{
        type: typeof SEARCH_ITEM_META.DEFAULT
        price: number
    }>

type SearchItemMetaDownload = SearchItemMetaBaseProps &
    Readonly<{
        type: typeof SEARCH_ITEM_META.Download
    }>

type SearchItemMetaInstalled = SearchItemMetaBaseProps &
    Readonly<{
        type: typeof SEARCH_ITEM_META.Installed
    }>

type SearchItemMetaUpcoming = SearchItemMetaBaseProps &
    Readonly<{
        type: typeof SEARCH_ITEM_META.Upcoming
    }>

type SearchItemMetaProps =
    | SearchItemMetaDefault
    | SearchItemMetaDownload
    | SearchItemMetaInstalled
    | SearchItemMetaUpcoming

const SearchItemMeta = (props: SearchItemMetaProps) => {
    const { recent } = props

    return (
        <div className={'flex items-center gap-4'}>
            <div className={'hidden md:inline-block'}>
                <SearchItemBadge {...props} />
            </div>
            {recent && (
                <>
                    <span
                        className={'hidden md:inline-block w-px h-6 bg-border'}
                    />
                    <button className={'rounded-sm focus-ring'}>
                        <XMarkIcon
                            className={
                                'size-4 md:size-5 text-muted-foreground cursor-pointer opacity-70 transition-opacity hover:opacity-100'
                            }
                        />
                        <span className={'sr-only'}>Remove from recent</span>
                    </button>
                </>
            )}
        </div>
    )
}

function SearchItemBadge(props: SearchItemMetaProps) {
    const { type } = props

    switch (type) {
        case SEARCH_ITEM_META.Download: {
            return <Badge className={'pointer-events-none'}>In library</Badge>
        }

        case SEARCH_ITEM_META.Installed: {
            return (
                <Badge className={'pointer-events-none'}>Ready to play</Badge>
            )
        }

        case SEARCH_ITEM_META.Upcoming: {
            return (
                <Badge className={'pointer-events-none'} variant={'outline'}>
                    Coming soon
                </Badge>
            )
        }

        default: {
            const { price } = props

            const isFree = price === 0

            if (isFree) {
                return (
                    <Badge
                        className={'pointer-events-none'}
                        variant={'outline'}
                    >
                        Free to play
                    </Badge>
                )
            }

            return (
                <div className={'flex items-center gap-2'}>
                    <span className={'text-xs font-medium'}>
                        {formatCurrency(price)}
                    </span>
                    <Button
                        className={
                            'size-9 hover:bg-primary hover:text-primary-foreground'
                        }
                        variant={'outline'}
                        size={'icon'}
                    >
                        <ShoppingBag className={'size-4'} />
                        <span className={'sr-only'}>Add to cart</span>
                    </Button>
                </div>
            )
        }
    }
}
