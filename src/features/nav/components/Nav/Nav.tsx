import React from 'react'

type NavProps = Readonly<{
    children: React.ReactNode
}>

export default function Nav(props: NavProps) {
    const { children } = props

    return <nav className={'flex items-center gap-10'}>{children}</nav>
}
