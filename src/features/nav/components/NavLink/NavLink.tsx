'use client'

import React from 'react'

import Link from 'next/link'

import { motion } from 'framer-motion'

import { cn } from '@/lib/utils'

import { useNavLink } from '@/features/nav/hooks'

import { setTransform } from '@/features/nav/utils'

type NavLinkProps = Readonly<
    Omit<React.ComponentProps<typeof Link>, 'children' | 'className'>
> & {
    children: (active: boolean) => React.ReactNode
}

export default function NavLink(props: NavLinkProps) {
    const { children, ...restProps } = props

    const { pathname, x, y, textX, textY } = useNavLink()

    const isActive = props.href === pathname

    function onPointerMove(evt: React.PointerEvent<HTMLDivElement>) {
        const item = evt.currentTarget
        setTransform(item, evt, x, y)
    }

    function onPointerLeave() {
        x.set(0)
        y.set(0)
    }

    return (
        <div className={'relative flex items-center h-full'}>
            <motion.div
                style={{ x, y }}
                className={'ease-elastic-out duration-1000'}
                onPointerMove={onPointerMove}
                onPointerLeave={onPointerLeave}
            >
                <Link
                    className={cn(
                        'inline-block p-2 rounded-md select-none focus-ring',
                        !isActive &&
                            'text-secondary-foreground hover:bg-accent hover:text-accent-foreground focus-visible:bg-accent focus-visible:text-accent-foreground',
                        isActive && 'bg-secondary text-primary',
                    )}
                    {...restProps}
                >
                    <motion.div
                        style={{
                            x: textX,
                            y: textY,
                        }}
                        className={'ease-elastic-out duration-1000'}
                    >
                        <span
                            className={
                                'flex items-center text-sm font-medium ease-linear duration-200'
                            }
                        >
                            {children(isActive)}
                        </span>
                    </motion.div>
                </Link>
            </motion.div>
            <ActiveIndicator active={isActive} />
        </div>
    )
}

type ActiveIndicatorProps = Readonly<{
    active: boolean
}>

function ActiveIndicator(props: ActiveIndicatorProps) {
    const { active } = props

    if (!active) {
        return null
    }

    return (
        <motion.div
            className={'absolute inset-x-0 bottom-0 flex justify-center'}
            layoutId={'underline'}
            transition={{ type: 'spring' }}
        >
            <span className={'w-3/4 h-1 bg-primary rounded-t-full'} />
        </motion.div>
    )
}
