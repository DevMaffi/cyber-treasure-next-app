import { usePathname } from 'next/navigation'

import { useMotionValue, useTransform } from 'framer-motion'

export function useNavLink() {
    const pathname = usePathname()

    const x = useMotionValue(0)
    const y = useMotionValue(0)

    const textX = useTransform(x, latest => latest * 0.5)
    const textY = useTransform(y, latest => latest * 0.5)

    return {
        pathname,
        x,
        y,
        textX,
        textY,
    }
}
