import React from 'react'

import type { MotionValue } from 'framer-motion'

export function setTransform(
    item: EventTarget & HTMLElement,
    event: React.PointerEvent,
    x: MotionValue,
    y: MotionValue,
) {
    const bounds = item.getBoundingClientRect()

    const relativeX = event.clientX - bounds.left
    const relativeY = event.clientY - bounds.top

    const xRange = mapRange({
        inputLower: 0,
        inputUpper: bounds.width,
    })(relativeX)

    const yRange = mapRange({
        inputLower: 0,
        inputUpper: bounds.height,
    })(relativeY)

    x.set(xRange * 10)
    y.set(yRange * 10)
}

type MapRangeProps = Readonly<{
    inputLower: number
    inputUpper: number
    outputLower?: number
    outputUpper?: number
}>

function mapRange(props: MapRangeProps) {
    const { inputLower, inputUpper, outputLower = -1, outputUpper = 1 } = props

    const INPUT_RANGE = inputUpper - inputLower
    const OUTPUT_RANGE = outputUpper - outputLower

    return function (value: number) {
        return (
            outputLower +
            (((value - inputLower) / INPUT_RANGE) * OUTPUT_RANGE || 0)
        )
    }
}
