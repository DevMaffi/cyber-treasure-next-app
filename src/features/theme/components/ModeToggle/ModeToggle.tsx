'use client'

import { useTheme } from 'next-themes'

import { MoonIcon, SunIcon } from '@heroicons/react/24/outline'

import { Button } from '@/components/ui/button'

import {
    DropdownMenu,
    DropdownMenuContent,
    DropdownMenuRadioGroup,
    DropdownMenuRadioItem,
    DropdownMenuTrigger,
} from '@/components/ui/dropdown-menu'

import { THEME_MODE } from '@/features/theme/types/theme'

export default function ModeToggle() {
    const { theme, setTheme } = useTheme()

    return (
        <DropdownMenu>
            <DropdownMenuTrigger asChild>
                <Button variant={'outline'} size={'icon'}>
                    <SunIcon
                        className={
                            'h-[1.2rem] w-[1.2rem] rotate-0 scale-100 transition-all dark:-rotate-90 dark:scale-0'
                        }
                    />
                    <MoonIcon
                        className={
                            'absolute h-[1.2rem] w-[1.2rem] rotate-90 scale-0 transition-all dark:rotate-0 dark:scale-100'
                        }
                    />
                    <span className={'sr-only'}>Toggle theme</span>
                </Button>
            </DropdownMenuTrigger>
            <DropdownMenuContent align={'end'}>
                <DropdownMenuRadioGroup value={theme} onValueChange={setTheme}>
                    <DropdownMenuRadioItem value={THEME_MODE.Light}>
                        Light
                    </DropdownMenuRadioItem>
                    <DropdownMenuRadioItem value={THEME_MODE.Dark}>
                        Dark
                    </DropdownMenuRadioItem>
                    <DropdownMenuRadioItem value={THEME_MODE.System}>
                        System
                    </DropdownMenuRadioItem>
                </DropdownMenuRadioGroup>
            </DropdownMenuContent>
        </DropdownMenu>
    )
}
