import { useTheme } from 'next-themes'

import { THEME_MODE } from '@/features/theme/types/theme'

export function useModeToggle() {
    const { resolvedTheme, setTheme } = useTheme()

    function toggleMode() {
        const isDarkMode = resolvedTheme === THEME_MODE.Dark

        if (isDarkMode) {
            setTheme(THEME_MODE.Light)
            return
        }

        setTheme(THEME_MODE.Dark)
    }

    return {
        resolvedTheme,
        toggleMode,
    }
}
