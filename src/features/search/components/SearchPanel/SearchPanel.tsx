'use client'

import { ArrowLongDownIcon, ArrowLongUpIcon } from '@heroicons/react/16/solid'

import { TypographyKbd } from '@/components/typography'

import {
    SearchDialog,
    SearchDialogCommand,
    SearchDialogFooter,
    SearchEmpty,
    SearchGroup,
    SearchInput,
    SearchItem,
    SearchList,
} from '@/components/SearchDialog'

import { useSearchPanel } from '@/features/search/hooks'

export default function SearchPanel() {
    const { open, setOpen } = useSearchPanel()

    return (
        <SearchDialog open={open} onOpenChange={setOpen}>
            <SearchInput placeholder={'Enter a search prompt'} />
            <SearchList>
                <SearchEmpty>No results found.</SearchEmpty>
                <SearchGroup heading={'Adventure'}>
                    <SearchItem
                        title={'Half-Life'}
                        category={'Action'}
                        rate={5}
                        previewUrl={
                            'https://cdn.akamai.steamstatic.com/steam/apps/70/header.jpg?t=1700269108'
                        }
                        meta={{ type: 'installed' }}
                    />
                    <SearchItem
                        title={"Baldur's Gate 3"}
                        category={'RPG'}
                        rate={4.8}
                        previewUrl={
                            'https://cdn.akamai.steamstatic.com/steam/apps/1086940/header.jpg?t=1713271288'
                        }
                        meta={{
                            type: 'default',
                            price: 45,
                        }}
                    />
                    <SearchItem
                        title={'Diablo® IV'}
                        category={'RPG'}
                        rate={4.3}
                        previewUrl={
                            'https://cdn.akamai.steamstatic.com/steam/apps/2344520/header.jpg?t=1713542896'
                        }
                        meta={{ type: 'download' }}
                    />
                    <SearchItem
                        title={'The Last of Us™ Part I'}
                        category={'Adventure'}
                        rate={4.7}
                        previewUrl={
                            'https://cdn.akamai.steamstatic.com/steam/apps/1888930/header.jpg?t=1705640438'
                        }
                        meta={{
                            type: 'default',
                            price: 80,
                        }}
                    />
                </SearchGroup>
                <SearchGroup heading={'Multiplayer'}>
                    <SearchItem
                        title={'Gray Zone Warfare'}
                        category={'Survival'}
                        previewUrl={
                            'https://cdn.akamai.steamstatic.com/steam/apps/2479810/header.jpg?t=1713527725'
                        }
                        meta={{ type: 'upcoming' }}
                    />
                    <SearchItem
                        title={'PUBG: BATTLEGROUNDS'}
                        category={'Battle Royale'}
                        rate={3.4}
                        previewUrl={
                            'https://cdn.akamai.steamstatic.com/steam/apps/578080/header.jpg?t=1712911294'
                        }
                        meta={{
                            type: 'default',
                            price: 0,
                        }}
                    />
                    <SearchItem
                        title={'Arma Reforger'}
                        category={'Simulator'}
                        rate={4.6}
                        previewUrl={
                            'https://cdn.akamai.steamstatic.com/steam/apps/107410/header.jpg?t=1709821285'
                        }
                        meta={{
                            type: 'default',
                            price: 30,
                        }}
                    />
                </SearchGroup>
            </SearchList>
            <SearchDialogFooter>
                <SearchDialogCommand label={'Move'}>
                    <TypographyKbd>
                        <ArrowLongUpIcon className={'size-2.5'} />
                    </TypographyKbd>
                    <TypographyKbd>
                        <ArrowLongDownIcon className={'size-2.5'} />
                    </TypographyKbd>
                </SearchDialogCommand>
                <SearchDialogCommand label={'Select'}>
                    <TypographyKbd>enter</TypographyKbd>
                </SearchDialogCommand>
            </SearchDialogFooter>
        </SearchDialog>
    )
}
