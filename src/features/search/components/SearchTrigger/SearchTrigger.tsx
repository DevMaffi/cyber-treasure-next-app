'use client'

import React from 'react'

import { MagnifyingGlassIcon } from '@heroicons/react/16/solid'

import { TypographyKbd } from '@/components/typography'

import { shortcuts } from '@/config/shortcuts'

import { cn } from '@/lib/utils'

import { useSearchPanelCtx } from '@/features/search/context'

type SearchTriggerProps = Readonly<
    Omit<React.ComponentProps<'button'>, 'children' | 'onClick'>
>

export default function SearchTrigger(props: SearchTriggerProps) {
    const { className, ...restProps } = props

    const { setOpen } = useSearchPanelCtx()

    function onOpen() {
        setOpen(true)
    }

    return (
        <button
            className={cn(
                'inline-flex justify-center items-center size-9 xl:min-w-64 xl:px-4 bg-background border border-input rounded-lg shadow-sm text-sm text-secondary-foreground select-none transition-colors focus-ring disabled:pointer-events-none disabled:opacity-50',
                className,
            )}
            onClick={onOpen}
            {...restProps}
        >
            <div className={'flex xl:flex-1 items-center'}>
                <MagnifyingGlassIcon className={'size-4 xl:mr-2'} />
                <span className={'sr-only xl:not-sr-only'}>Tab to search</span>
            </div>
            <Shortcut />
        </button>
    )
}

function Shortcut() {
    const keys = shortcuts.searchPanel.keys

    return (
        <div className={'hidden xl:flex items-center gap-1.5'}>
            {keys.map(key => {
                return (
                    <TypographyKbd key={key} className={'capitalize'}>
                        {key}
                    </TypographyKbd>
                )
            })}
        </div>
    )
}
