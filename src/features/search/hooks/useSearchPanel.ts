import { useEffect } from 'react'

import { shortcuts } from '@/config/shortcuts'

import { useSearchPanelCtx } from '@/features/search/context'

export function useSearchPanel() {
    const { open, setOpen } = useSearchPanelCtx()

    useEffect(() => {
        function down(evt: KeyboardEvent) {
            if (shortcuts.searchPanel.matcher(evt)) {
                evt.preventDefault()
                setOpen(open => !open)
            }
        }

        document.addEventListener('keydown', down)

        return () => document.removeEventListener('keydown', down)
    })

    return {
        open,
        setOpen,
    }
}
