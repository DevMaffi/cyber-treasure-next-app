'use client'

import React, { useContext, useState } from 'react'

type SearchPanelCtxProps = Readonly<{
    open: boolean
    setOpen: React.Dispatch<React.SetStateAction<boolean>>
}>

const SearchPanelCtx = React.createContext<SearchPanelCtxProps | null>(null)

type SearchPanelCtxProviderProps = Readonly<{
    children: React.ReactNode
}>

export default function SearchPanelCtxProvider(
    props: SearchPanelCtxProviderProps,
) {
    const { children } = props

    const [open, setOpen] = useState(false)

    return (
        <SearchPanelCtx.Provider
            value={{
                open,
                setOpen,
            }}
        >
            {children}
        </SearchPanelCtx.Provider>
    )
}

export function useSearchPanelCtx() {
    const context = useContext(SearchPanelCtx)

    if (!context) {
        throw new Error(
            'useSearchPanelCtx must be used within a SearchPanelCtxProvider',
        )
    }

    return context
}
