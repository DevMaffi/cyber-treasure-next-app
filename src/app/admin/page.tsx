import {
    getGamesData,
    getSalesData,
    getUsersData,
} from '@/app/admin/_actions/dashboard'

import { formatCurrency, formatNumber } from '@/lib/utils/formatters'

import { DashboardCard } from '@/app/admin/_features/dashboard'

export default async function AdminDashboard() {
    const [salesData, usersData, gamesData] = await Promise.all([
        getSalesData(),
        getUsersData(),
        getGamesData(),
    ])

    return (
        <main>
            <div className={'grid grid-cols-dashboard gap-4'}>
                <DashboardCard
                    title={'Sales'}
                    subtitle={`${formatNumber(salesData.numberOfSales)} Orders`}
                    body={formatCurrency(salesData.amount)}
                />
                <DashboardCard
                    title={'Customers'}
                    subtitle={`${formatCurrency(usersData.averageValuePerUser)} Average Value`}
                    body={formatNumber(usersData.userCount)}
                />
                <DashboardCard
                    title={'Published Games'}
                    subtitle={`${formatNumber(gamesData.categoryCount)} Categories`}
                    body={formatNumber(gamesData.gameCount)}
                />
            </div>
        </main>
    )
}
