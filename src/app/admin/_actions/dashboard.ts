'use server'

import db from '@/lib/db'

export async function getSalesData() {
    const data = await db.order.aggregate({
        _sum: { pricePaidInCents: true },
        _count: true,
    })

    return {
        amount: (data._sum.pricePaidInCents || 0) / 100,
        numberOfSales: data._count,
    }
}

export async function getUsersData() {
    const [userCount, orderData] = await Promise.all([
        db.user.count(),
        db.order.aggregate({
            _sum: { pricePaidInCents: true },
        }),
    ])

    const averageValuePerUser =
        userCount > 0
            ? (orderData._sum.pricePaidInCents || 0) / userCount / 100
            : 0

    return {
        userCount,
        averageValuePerUser,
    }
}

export async function getGamesData() {
    const [gameCount, categoryCount] = await Promise.all([
        db.game.count(),
        db.category.count(),
    ])

    return {
        gameCount,
        categoryCount,
    }
}
