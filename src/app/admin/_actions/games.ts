'use server'

import fs from 'fs/promises'

import { redirect } from 'next/navigation'

import { z } from 'zod'

import { adminRoutes } from '@/app/admin/_config/routes'

import db from '@/lib/db'

export async function getGamesCategories() {
    return db.category.findMany({
        orderBy: {
            name: 'asc',
        },
        select: {
            id: true,
            name: true,
            isPublished: true,
        },
    })
}

const fileSchema = z.instanceof(File, { message: 'Required' })

const previewSchema = fileSchema.refine(
    file => file.size === 0 || file.type.startsWith('image/'),
)

const addSchema = z.object({
    title: z.string().min(1).trim(),
    category: z.string().min(1, 'Required'),
    priceInCents: z.coerce.number().int().min(0).default(0),
    description: z.string().min(1),
    file: fileSchema.refine(file => file.size > 0, 'File is empty'),
    preview: previewSchema.refine(file => file.size > 0, 'File is empty'),
    isPublished: z.coerce.boolean().optional(),
})

export async function addGame(_: unknown, formData: FormData) {
    const result = addSchema.safeParse(Object.fromEntries(formData.entries()))

    if (!result.success) {
        return result.error.formErrors.fieldErrors
    }

    const data = result.data

    await fs.mkdir('files/games', { recursive: true })
    const filePath = `files/games/${crypto.randomUUID()}-${data.file.name}`
    await fs.writeFile(filePath, Buffer.from(await data.file.arrayBuffer()))

    await fs.mkdir('public/games/preview', { recursive: true })
    const previewPath = `/games/preview/${crypto.randomUUID()}-${data.preview.name}`

    await fs.writeFile(
        `public${previewPath}`,
        Buffer.from(await data.preview.arrayBuffer()),
    )

    redirect(adminRoutes.games.pathname)
}
