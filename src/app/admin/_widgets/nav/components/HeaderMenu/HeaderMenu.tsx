import { adminRoutes } from '@/app/admin/_config/routes'

import { NavLink } from '@/app/admin/_features/nav'

export default function HeaderMenu() {
    return (
        <>
            <NavLink href={adminRoutes.dashboard.pathname}>
                {adminRoutes.dashboard.label}
            </NavLink>
            <NavLink href={adminRoutes.games.pathname}>
                {adminRoutes.games.label}
            </NavLink>
            <NavLink href={adminRoutes.users.pathname}>
                {adminRoutes.users.label}
            </NavLink>
            <NavLink href={adminRoutes.orders.pathname}>
                {adminRoutes.orders.label}
            </NavLink>
        </>
    )
}
