import Link from 'next/link'

import { Cog6ToothIcon, HomeIcon } from '@heroicons/react/24/outline'

import { Button } from '@/components/ui/button'

import { Logo } from '@/app/admin/_components'

import { adminRoutes } from '@/app/admin/_config/routes'

import { Nav } from '@/app/admin/_features/nav'

import { HeaderMenu } from '@/app/admin/_widgets/nav/components/HeaderMenu'

export default function Header() {
    const STORE_HOME_LINK = process.env.STORE_HOME_LINK ?? '/not-found'

    return (
        <header
            className={
                'sticky top-0 inset-x-0 flex justify-between items-center py-2.5 px-7 bg-secondary text-secondary-foreground'
            }
        >
            <Logo />
            <div
                className={
                    'absolute bottom-0 left-1/2 flex -translate-x-1/2 -z-10'
                }
            >
                <Nav>
                    <HeaderMenu />
                </Nav>
            </div>
            <div className={'flex items-center gap-5'}>
                <Button
                    className={
                        'rounded-full hover:bg-primary hover:text-primary-foreground'
                    }
                    variant={'outline'}
                    size={'icon'}
                    asChild
                >
                    <Link href={STORE_HOME_LINK} target={'_blank'}>
                        <HomeIcon className={'size-5'} />
                    </Link>
                </Button>
                <Button
                    className={
                        'group rounded-full hover:bg-primary hover:text-primary-foreground'
                    }
                    variant={'outline'}
                    size={'icon'}
                    asChild
                >
                    <Link href={adminRoutes.settings.pathname}>
                        <Cog6ToothIcon
                            className={'size-5 group-hover:animate-spin-in'}
                        />
                    </Link>
                </Button>
            </div>
        </header>
    )
}
