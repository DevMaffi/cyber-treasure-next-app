import Link from 'next/link'

import { PlusIcon } from '@heroicons/react/16/solid'

import { Button } from '@/components/ui/button'

import { PageHeader } from '@/app/admin/_components'

import { adminRoutes } from '@/app/admin/_config/routes'

export default function Games() {
    return (
        <main>
            <header className={'flex justify-between gap-4'}>
                <PageHeader>{adminRoutes.games.label}</PageHeader>
                <Button size={'sm'} asChild>
                    <Link href={adminRoutes.newGame.pathname}>
                        <PlusIcon className={'size-4 mr-1'} />
                        <span>{adminRoutes.newGame.label}</span>
                    </Link>
                </Button>
            </header>
        </main>
    )
}
