import { Label } from '@/components/ui/label'
import { Switch } from '@/components/ui/switch'

export default function GamePublishSwitch() {
    return (
        <div
            className={
                'flex justify-between items-center p-4 rounded-lg border'
            }
        >
            <div className={'space-y-0.5'}>
                <Label className={'text-base'} htmlFor={'isPublished'}>
                    Publish <span className={'text-primary'}>(optional)</span>
                </Label>
                <p className={'text-sm text-muted-foreground'}>
                    Make your game available to others (after saving).
                </p>
            </div>
            <Switch id={'isPublished'} name={'isPublished'} />
        </div>
    )
}
