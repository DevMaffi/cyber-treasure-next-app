'use client'

import React, { useState } from 'react'
import { useFormState } from 'react-dom'

import { useRouter } from 'next/navigation'

import { Label } from '@/components/ui/label'
import { Input } from '@/components/ui/input'
import { Textarea } from '@/components/ui/textarea'
import { Button } from '@/components/ui/button'

import {
    FormBtnGroup,
    FormItem,
    FormMessage,
    SubmitButton,
} from '@/components/form'

import { adminRoutes } from '@/app/admin/_config/routes'

import type { GameCategory } from '@/model/category'

import { addGame } from '@/app/admin/_actions/games'

import { formatCurrency } from '@/lib/utils/formatters'

import { GameCategorySelect } from '@/app/admin/games/new/_components/GameCategorySelect'
import { GamePublishSwitch } from '@/app/admin/games/new/_components/GamePublishSwitch'

type GameFormProps = Readonly<{
    categories: GameCategory[]
}>

export default function GameForm(props: GameFormProps) {
    const { categories } = props

    const [priceInCents, setPriceInCents] = useState<number | string>('')

    const [error, action] = useFormState(addGame, {})

    const router = useRouter()

    function onPriceInCentsChange(evt: React.ChangeEvent<HTMLInputElement>) {
        const input = evt.target
        setPriceInCents(Number(input.value) ?? '')
    }

    function onWheelInputBlur(evt: React.WheelEvent<HTMLInputElement>) {
        const input = evt.currentTarget
        input.blur()
    }

    function onFormCancel() {
        router.push(adminRoutes.games.pathname)
    }

    return (
        <form action={action} className={'space-y-8'}>
            <FormItem>
                <Label htmlFor={'title'}>Title</Label>
                <Input type={'text'} id={'title'} name={'title'} />
                <FormMessage variant={'destructive'} message={error.title} />
            </FormItem>
            <FormItem>
                <Label>Category</Label>
                <GameCategorySelect categories={categories} />
                <FormMessage variant={'destructive'} message={error.category} />
            </FormItem>
            <FormItem>
                <Label htmlFor={'priceInCents'}>Price In Cents</Label>
                <Input
                    type={'number'}
                    id={'priceInCents'}
                    name={'priceInCents'}
                    value={priceInCents}
                    onChange={onPriceInCentsChange}
                    onWheel={onWheelInputBlur}
                />
                <FormMessage
                    message={formatCurrency((Number(priceInCents) || 0) / 100)}
                />
                <FormMessage
                    variant={'destructive'}
                    message={error.priceInCents}
                />
            </FormItem>
            <FormItem>
                <Label htmlFor={'description'}>Description</Label>
                <Textarea id={'description'} name={'description'} />
                <FormMessage
                    variant={'destructive'}
                    message={error.description}
                />
            </FormItem>
            <FormItem>
                <Label htmlFor={'file'}>File</Label>
                <Input type={'file'} id={'file'} name={'file'} />
                <FormMessage variant={'destructive'} message={error.file} />
            </FormItem>
            <FormItem>
                <Label htmlFor={'preview'}>Preview</Label>
                <Input type={'file'} id={'preview'} name={'preview'} />
                <FormMessage variant={'destructive'} message={error.preview} />
            </FormItem>
            <FormItem>
                <Label htmlFor={'isPublished'}>Publish Game</Label>
                <GamePublishSwitch />
                <FormMessage
                    variant={'destructive'}
                    message={error.isPublished}
                />
            </FormItem>
            <FormBtnGroup>
                <Button
                    type={'button'}
                    variant={'secondary'}
                    size={'sm'}
                    onClick={onFormCancel}
                >
                    Cancel
                </Button>
                <SubmitButton label={'Save'} />
            </FormBtnGroup>
        </form>
    )
}
