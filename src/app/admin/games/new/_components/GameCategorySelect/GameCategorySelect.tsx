import {
    Select,
    SelectContent,
    SelectItem,
    SelectTrigger,
    SelectValue,
} from '@/components/ui/select'

import { Badge } from '@/components/ui/badge'

import type { GameCategory } from '@/model/category'

type GameCategorySelectProps = Readonly<{
    categories: GameCategory[]
}>

export default function GameCategorySelect(props: GameCategorySelectProps) {
    const categories: GameCategory[] = [
        {
            id: '1',
            name: 'Adventure',
            isPublished: false,
        },
        ...props.categories,
    ]

    return (
        <Select name={'category'}>
            <SelectTrigger>
                <SelectValue placeholder={'Select a category'} />
            </SelectTrigger>
            <SelectContent>
                {categories.map(category => {
                    return (
                        <SelectItem key={category.id} value={category.id}>
                            <span>{category.name}</span>
                            {!category.isPublished && (
                                <Badge className={'ml-4'} variant={'outline'}>
                                    draft
                                </Badge>
                            )}
                        </SelectItem>
                    )
                })}
            </SelectContent>
        </Select>
    )
}
