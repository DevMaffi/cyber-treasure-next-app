import { PageHeader } from '@/app/admin/_components'
import { GameForm } from '@/app/admin/games/new/_components'

import { adminRoutes } from '@/app/admin/_config/routes'

import { getGamesCategories } from '@/app/admin/_actions/games'

export default async function NewGame() {
    const categories = await getGamesCategories()

    return (
        <main>
            <PageHeader>{adminRoutes.newGame.label}</PageHeader>
            <GameForm categories={categories} />
        </main>
    )
}
