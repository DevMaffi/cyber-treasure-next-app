'use client'

import React from 'react'

import Link from 'next/link'

import { usePathname } from 'next/navigation'

import { motion } from 'framer-motion'

import { cn } from '@/lib/utils'

type NavLinkProps = Readonly<
    Omit<React.ComponentProps<typeof Link>, 'className'>
>

export default function NavLink(props: NavLinkProps) {
    const { children, ...restProps } = props

    const pathname = usePathname()

    const isActive = props.href === pathname

    return (
        <div className={'relative'}>
            <Link
                className={cn(
                    'inline-block py-3.5 px-4 select-none transition-colors',
                    !isActive &&
                        'hover:text-primary focus-visible:text-primary',
                    isActive && 'text-foreground',
                )}
                {...restProps}
            >
                <span className={'inline-block -translate-y-1'}>
                    {children}
                </span>
            </Link>
            {isActive && (
                <motion.span
                    className={
                        'absolute inset-0 bg-background rounded-t-lg -z-10 before:content[""] before:absolute before:end-full before:bottom-0 before:size-3.5 before:bg-secondary before:rounded-br-full before:shadow-[5px_5px_0px_5px_theme(colors.background)] after:content[""] after:absolute after:bottom-0 after:start-full after:size-3.5 after:bg-secondary after:rounded-bl-full after:shadow-[-5px_5px_0px_5px_theme(colors.background)]'
                    }
                    layoutId={'outline'}
                />
            )}
        </div>
    )
}
