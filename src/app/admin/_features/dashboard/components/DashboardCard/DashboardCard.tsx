import {
    Card,
    CardContent,
    CardDescription,
    CardHeader,
    CardTitle,
} from '@/components/ui/card'

type DashboardCardProps = Readonly<{
    title: string
    subtitle: string
    body: string
}>

export default function DashboardCard(props: DashboardCardProps) {
    const { title, subtitle, body } = props

    return (
        <Card>
            <CardHeader>
                <CardTitle>{title}</CardTitle>
                <CardDescription>{subtitle}</CardDescription>
            </CardHeader>
            <CardContent>
                <p>{body}</p>
            </CardContent>
        </Card>
    )
}
