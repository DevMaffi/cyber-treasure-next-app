import type { Routes } from '@/types/routes'

export const adminRoutes: Routes = {
    dashboard: {
        pathname: '/admin',
        label: 'Dashboard',
    },

    games: {
        pathname: '/admin/games',
        label: 'Games',
    },

    newGame: {
        pathname: '/admin/games/new',
        label: 'Add Game',
    },

    users: {
        pathname: '/admin/users',
        label: 'Customers',
    },

    orders: {
        pathname: '/admin/orders',
        label: 'Sales',
    },

    settings: {
        pathname: '/admin/settings',
        label: 'Settings',
    },
}
