import React from 'react'

import { Header } from '@/app/admin/_widgets/nav'

export const dynamic = 'force-dynamic'

type AdminLayoutProps = Readonly<{
    children: React.ReactNode
}>

export default function AdminLayout(props: AdminLayoutProps) {
    const { children } = props

    return (
        <>
            <Header />
            <div className={'container my-12'}>{children}</div>
        </>
    )
}
