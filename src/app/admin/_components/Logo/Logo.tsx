import Link from 'next/link'

import { BeakerIcon } from '@heroicons/react/24/solid'
import { ArrowUpRightIcon } from '@heroicons/react/16/solid'

import { Badge } from '@/components/ui/badge'

import { adminRoutes } from '@/app/admin/_config/routes'

export default function Logo() {
    const APP_VERSION = process.env.npm_package_version

    const GITHUB_REPO_URL = process.env.GITHUB_REPO_URL ?? '/not-found'

    return (
        <div className={'inline-flex items-center gap-4'}>
            <Link href={adminRoutes.dashboard.pathname}>
                <BeakerIcon className={'size-6'} />
            </Link>
            <Link href={GITHUB_REPO_URL} target={'_blank'} tabIndex={-1}>
                <Badge>
                    <span>{APP_VERSION}</span>
                    <ArrowUpRightIcon
                        className={
                            'size-3 translate-x-1 -translate-y-[0.1875rem]'
                        }
                    />
                </Badge>
            </Link>
        </div>
    )
}
