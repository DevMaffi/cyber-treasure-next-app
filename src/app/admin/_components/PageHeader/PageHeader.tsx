import React from 'react'

type PageHeaderProps = Readonly<{
    children: React.ReactNode
}>

export default function PageHeader(props: PageHeaderProps) {
    const { children } = props

    return <h1 className={'mb-10 text-3xl'}>{children}</h1>
}
