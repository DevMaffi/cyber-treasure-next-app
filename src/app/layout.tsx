import React from 'react'

import type { Metadata } from 'next'
import { Inter as FontSans } from 'next/font/google'

import { RootProvider } from '@/providers'

import { RootShortcuts } from '@/components'

import { cn } from '@/lib/utils'

import './globals.css'

const fontSans = FontSans({
    subsets: ['latin'],
    variable: '--font-sans',
})

export const metadata: Metadata = {
    title: 'Cyber-Treasure',
}

type RootLayoutProps = Readonly<{
    children: React.ReactNode
}>

export default function RootLayout(props: RootLayoutProps) {
    const { children } = props

    return (
        <html lang={'en'} suppressHydrationWarning>
            <body
                className={cn(
                    'min-h-screen bg-background font-sans antialiased',
                    fontSans.variable,
                )}
            >
                <RootProvider>
                    {children}
                    <RootShortcuts />
                </RootProvider>
            </body>
        </html>
    )
}
