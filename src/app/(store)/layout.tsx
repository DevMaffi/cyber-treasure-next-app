import React from 'react'

import { StoreProvider } from '@/providers'

import { StoreShortcuts } from '@/components'

import { Header } from '@/widgets/nav'

type StoreLayoutProps = Readonly<{
    children: React.ReactNode
}>

export default function StoreLayout(props: StoreLayoutProps) {
    const { children } = props

    return (
        <StoreProvider>
            <Header />
            {children}
            <StoreShortcuts />
        </StoreProvider>
    )
}
