export type GameCategory = Readonly<{
    id: string
    name: string
    isPublished: boolean
}>
