import type { Routes } from '@/types/routes'

export const routes: Routes = {
    home: {
        pathname: '/',
        label: 'Home',
    },

    lib: {
        pathname: '/library',
        label: 'Library',
    },

    charts: {
        pathname: '/charts',
        label: 'Charts',
    },

    liked: {
        pathname: '/wishlisted',
        label: 'Wishlisted',
    },

    cart: {
        pathname: '/cart',
        label: 'Cart',
    },

    profile: {
        pathname: '/profile',
        label: 'Profile',
    },

    settings: {
        pathname: '/settings',
        label: 'Settings',
    },
}
