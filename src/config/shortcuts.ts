import type { Shortcuts } from '@/types/shortcuts'

import { builder } from '@/lib/shortcuts'

export const shortcuts: Shortcuts = {
    searchPanel: builder().ctrl().key('k').bind(),
    profile: builder().ctrl().alt().key('p').bind(),
    theme: builder().ctrl().key('b').bind(),
    settings: builder().ctrl().alt().key('s').bind(),
}
