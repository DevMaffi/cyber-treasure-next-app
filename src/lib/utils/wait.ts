export function wait(duration = 500) {
    return new Promise(resolve => {
        setTimeout(resolve, duration)
    })
}
