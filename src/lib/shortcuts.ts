import { KEY } from '@/enums/keyboard'

export function builder() {
    const keys: string[] = []

    function ctrl() {
        keys.push(KEY.Ctrl)
        return chain
    }

    function alt() {
        keys.push(KEY.Alt)
        return chain
    }

    function key(key: string) {
        keys.push(key.toLowerCase())
        return chain
    }

    function bind() {
        const label = keys.join(' ')

        return {
            keys,
            label,
            matcher: matcher(keys),
        }
    }

    const chain = {
        ctrl,
        alt,
        key,
        bind,
    }

    return chain
}

function matcher(keys: string[]) {
    return function (evt: KeyboardEvent) {
        return keys.every(key => {
            switch (key) {
                case KEY.Ctrl:
                    return evt.ctrlKey || evt.metaKey

                case KEY.Alt:
                    return evt.altKey

                default:
                    return evt.key === key
            }
        })
    }
}
