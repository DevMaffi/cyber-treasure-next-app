import { useEffect } from 'react'

import { shortcuts } from '@/config/shortcuts'

import { useModeToggle } from '@/features/theme'

export function useRootShortcuts() {
    const { toggleMode } = useModeToggle()

    useEffect(() => {
        function down(evt: KeyboardEvent) {
            if (shortcuts.theme.matcher(evt)) {
                evt.preventDefault()
                toggleMode()
            }
        }

        document.addEventListener('keydown', down)

        return () => document.removeEventListener('keydown', down)
    })
}
