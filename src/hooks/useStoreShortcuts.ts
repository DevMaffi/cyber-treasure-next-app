import { useEffect } from 'react'

import { useRouter } from 'next/navigation'

import { routes } from '@/config/routes'
import { shortcuts } from '@/config/shortcuts'

import { useSearchPanelCtx } from '@/features/search'

export function useStoreShortcuts() {
    const router = useRouter()

    const { setOpen } = useSearchPanelCtx()

    useEffect(() => {
        function down(evt: KeyboardEvent) {
            if (shortcuts.profile.matcher(evt)) {
                evt.preventDefault()

                router.push(routes.profile.pathname)
                setOpen(false)
            }

            if (shortcuts.settings.matcher(evt)) {
                evt.preventDefault()

                router.push(routes.settings.pathname)
                setOpen(false)
            }
        }

        document.addEventListener('keydown', down)

        return () => document.removeEventListener('keydown', down)
    })
}
