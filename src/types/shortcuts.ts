export type Shortcuts = Readonly<Record<string, Shortcut>>

export type Shortcut = Readonly<{
    keys: string[]
    label: string
    matcher: (evt: KeyboardEvent) => boolean
}>
