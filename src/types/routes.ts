export type Routes = Readonly<Record<string, Route>>

export type Route = Readonly<{
    pathname: string
    label: string
}>
