'use client'

import React from 'react'

import {
    FireIcon,
    HeartIcon,
    HomeIcon,
    Squares2X2Icon,
} from '@heroicons/react/24/outline'

import {
    FireIcon as FireSolidIcon,
    HeartIcon as HeartSolidIcon,
    HomeIcon as HomeSolidIcon,
    Squares2X2Icon as Squares2X2SolidIcon,
} from '@heroicons/react/24/solid'

import { routes } from '@/config/routes'

import { NavLink } from '@/features/nav'

export default function HeaderMenu() {
    return (
        <>
            {renderNavLink({
                pathname: routes.home.pathname,
                label: routes.home.label,
                icons: {
                    default: <HomeIcon />,
                    active: <HomeSolidIcon />,
                },
            })}
            {renderNavLink({
                pathname: routes.lib.pathname,
                label: routes.lib.label,
                icons: {
                    default: <Squares2X2Icon />,
                    active: <Squares2X2SolidIcon />,
                },
            })}
            {renderNavLink({
                pathname: routes.charts.pathname,
                label: routes.charts.label,
                icons: {
                    default: <FireIcon />,
                    active: <FireSolidIcon />,
                },
            })}
            {renderNavLink({
                pathname: routes.liked.pathname,
                label: routes.liked.label,
                icons: {
                    default: <HeartIcon />,
                    active: <HeartSolidIcon />,
                },
            })}
        </>
    )
}

type RenderNavLinkProps = Readonly<{
    pathname: string
    label: string
    icons: Readonly<{
        default: React.ReactNode
        active: React.ReactNode
    }>
}>

function renderNavLink(props: RenderNavLinkProps) {
    const { pathname, label, icons } = props

    return (
        <NavLink href={pathname}>
            {active => {
                return (
                    <>
                        <div className={'size-5 xl:hidden'}>
                            {active ? icons.active : icons.default}
                            <span className={'sr-only'}>{label}</span>
                        </div>
                        <span className={'hidden xl:inline'}>{label}</span>
                    </>
                )
            }}
        </NavLink>
    )
}
