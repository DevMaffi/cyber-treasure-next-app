import { Logo } from '@/components'

import { ModeToggle } from '@/features/theme'
import { Nav } from '@/features/nav'
import { SearchPanel, SearchTrigger } from '@/features/search'

import { HeaderMenu } from '@/widgets/nav/components/HeaderMenu'

export default function Header() {
    return (
        <>
            <header
                className={
                    'sticky top-0 inset-x-0 flex justify-between items-center h-header py-4 px-5 xl:px-7 bg-background/60 backdrop-blur-sm border-b'
                }
            >
                <div className={'flex items-center gap-6 xl:gap-10'}>
                    <Logo />
                    <SearchTrigger />
                </div>
                <div
                    className={
                        'absolute inset-y-0 left-1/2 hidden sm:flex -translate-x-1/2 -z-10'
                    }
                >
                    <Nav>
                        <HeaderMenu />
                    </Nav>
                </div>
                <div className={'hidden xl:inline-block'}>
                    <ModeToggle />
                </div>
            </header>
            <SearchPanel />
        </>
    )
}
