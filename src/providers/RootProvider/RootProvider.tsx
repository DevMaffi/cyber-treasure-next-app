'use client'

import React from 'react'

import { ThemeProvider } from '@/providers/ThemeProvider'

type RootProviderProps = Readonly<{
    children: React.ReactNode
}>

export default function RootProvider(props: RootProviderProps) {
    const { children } = props

    return (
        <ThemeProvider
            attribute={'class'}
            defaultTheme={'system'}
            enableSystem
            disableTransitionOnChange
        >
            {children}
        </ThemeProvider>
    )
}
