'use client'

import React from 'react'

import { SearchPanelCtxProvider } from '@/features/search'

type StoreProviderProps = Readonly<{
    children: React.ReactNode
}>

export default function StoreProvider(props: StoreProviderProps) {
    const { children } = props

    return <SearchPanelCtxProvider>{children}</SearchPanelCtxProvider>
}
