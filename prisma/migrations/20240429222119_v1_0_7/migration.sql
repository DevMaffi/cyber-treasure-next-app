/*
  Warnings:

  - You are about to drop the column `cartId` on the `Order` table. All the data in the column will be lost.
  - You are about to drop the column `pricePaidInCents` on the `Order` table. All the data in the column will be lost.
  - You are about to drop the column `favorite` on the `UsersOnGames` table. All the data in the column will be lost.
  - You are about to drop the `Cart` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `GamesOnCart` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Ownership` table. If the table is not empty, all the data it contains will be lost.
  - A unique constraint covering the columns `[userId,name]` on the table `Payment` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `balanceInCents` to the `Order` table without a default value. This is not possible if the table is not empty.
  - Added the required column `paymentMethod` to the `Order` table without a default value. This is not possible if the table is not empty.
  - Added the required column `totalPaidInCents` to the `Order` table without a default value. This is not possible if the table is not empty.
  - Added the required column `name` to the `Payment` table without a default value. This is not possible if the table is not empty.
  - Made the column `wishlisted` on table `UsersOnGames` required. This step will fail if there are existing NULL values in that column.

*/
-- DropForeignKey
ALTER TABLE "Cart" DROP CONSTRAINT "Cart_userId_fkey";

-- DropForeignKey
ALTER TABLE "GamesOnCart" DROP CONSTRAINT "GamesOnCart_cartId_fkey";

-- DropForeignKey
ALTER TABLE "GamesOnCart" DROP CONSTRAINT "GamesOnCart_gameId_fkey";

-- DropForeignKey
ALTER TABLE "Order" DROP CONSTRAINT "Order_cartId_fkey";

-- DropForeignKey
ALTER TABLE "Order" DROP CONSTRAINT "Order_paymentId_fkey";

-- DropForeignKey
ALTER TABLE "Ownership" DROP CONSTRAINT "Ownership_gameId_fkey";

-- DropForeignKey
ALTER TABLE "Ownership" DROP CONSTRAINT "Ownership_orderId_fkey";

-- DropForeignKey
ALTER TABLE "Ownership" DROP CONSTRAINT "Ownership_ownerId_fkey";

-- DropIndex
DROP INDEX "Order_cartId_key";

-- DropIndex
DROP INDEX "Payment_userId_key";

-- AlterTable
ALTER TABLE "Order" DROP COLUMN "cartId",
DROP COLUMN "pricePaidInCents",
ADD COLUMN     "balanceInCents" INTEGER NOT NULL,
ADD COLUMN     "paymentMethod" "PaymentType" NOT NULL,
ADD COLUMN     "totalPaidInCents" INTEGER NOT NULL;

-- AlterTable
ALTER TABLE "Payment" ADD COLUMN     "name" TEXT NOT NULL,
ALTER COLUMN "balanceInCents" SET DEFAULT 50000;

-- AlterTable
ALTER TABLE "Preferences" ALTER COLUMN "initialAppPage" DROP NOT NULL;

-- AlterTable
ALTER TABLE "UsersOnGames" DROP COLUMN "favorite",
ALTER COLUMN "wishlisted" SET NOT NULL,
ALTER COLUMN "wishlisted" DROP DEFAULT;

-- DropTable
DROP TABLE "Cart";

-- DropTable
DROP TABLE "GamesOnCart";

-- DropTable
DROP TABLE "Ownership";

-- CreateTable
CREATE TABLE "GamesOnCarts" (
    "userId" TEXT NOT NULL,
    "gameId" TEXT NOT NULL,

    CONSTRAINT "GamesOnCarts_pkey" PRIMARY KEY ("userId","gameId")
);

-- CreateTable
CREATE TABLE "GamesOnOrders" (
    "gameId" TEXT NOT NULL,
    "orderId" TEXT NOT NULL,
    "ownerId" TEXT NOT NULL,
    "pricePaidInCents" INTEGER NOT NULL,
    "installed" BOOLEAN NOT NULL DEFAULT false,
    "favorite" BOOLEAN NOT NULL DEFAULT false,

    CONSTRAINT "GamesOnOrders_pkey" PRIMARY KEY ("gameId","orderId")
);

-- CreateIndex
CREATE UNIQUE INDEX "Payment_userId_name_key" ON "Payment"("userId", "name");

-- AddForeignKey
ALTER TABLE "GamesOnCarts" ADD CONSTRAINT "GamesOnCarts_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "GamesOnCarts" ADD CONSTRAINT "GamesOnCarts_gameId_fkey" FOREIGN KEY ("gameId") REFERENCES "Game"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Order" ADD CONSTRAINT "Order_paymentId_fkey" FOREIGN KEY ("paymentId") REFERENCES "Payment"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "GamesOnOrders" ADD CONSTRAINT "GamesOnOrders_gameId_fkey" FOREIGN KEY ("gameId") REFERENCES "Game"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "GamesOnOrders" ADD CONSTRAINT "GamesOnOrders_orderId_fkey" FOREIGN KEY ("orderId") REFERENCES "Order"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "GamesOnOrders" ADD CONSTRAINT "GamesOnOrders_ownerId_fkey" FOREIGN KEY ("ownerId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
