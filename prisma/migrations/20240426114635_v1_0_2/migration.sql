/*
  Warnings:

  - You are about to drop the column `previewUrl` on the `Game` table. All the data in the column will be lost.
  - You are about to drop the column `publishedAt` on the `Game` table. All the data in the column will be lost.
  - You are about to drop the column `publishedById` on the `Game` table. All the data in the column will be lost.
  - Added the required column `isPublished` to the `Category` table without a default value. This is not possible if the table is not empty.
  - Added the required column `createdById` to the `Game` table without a default value. This is not possible if the table is not empty.
  - Added the required column `description` to the `Game` table without a default value. This is not possible if the table is not empty.
  - Added the required column `filePath` to the `Game` table without a default value. This is not possible if the table is not empty.
  - Added the required column `previewPath` to the `Game` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "Game" DROP CONSTRAINT "Game_publishedById_fkey";

-- AlterTable
ALTER TABLE "Category" ADD COLUMN     "isPublished" BOOLEAN NOT NULL;

-- AlterTable
ALTER TABLE "Game" DROP COLUMN "previewUrl",
DROP COLUMN "publishedAt",
DROP COLUMN "publishedById",
ADD COLUMN     "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
ADD COLUMN     "createdById" TEXT NOT NULL,
ADD COLUMN     "description" TEXT NOT NULL,
ADD COLUMN     "filePath" TEXT NOT NULL,
ADD COLUMN     "isAvaliableForPurchase" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "previewPath" TEXT NOT NULL;

-- AddForeignKey
ALTER TABLE "Game" ADD CONSTRAINT "Game_createdById_fkey" FOREIGN KEY ("createdById") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
