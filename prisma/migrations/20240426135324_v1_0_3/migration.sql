/*
  Warnings:

  - You are about to drop the column `isAvaliableForPurchase` on the `Game` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Game" DROP COLUMN "isAvaliableForPurchase",
ADD COLUMN     "isPublished" BOOLEAN NOT NULL DEFAULT false;
